import { find } from 'better-sqlite3-proxy'
import { DBInstance } from 'better-sqlite3-schema'
import { BlockProposal, BlockProposalResponse, MsgType, TxnType } from './data'
import { createProxy } from './db.proxy'
import { IOServer } from './io'
import { KeyPair } from './key'
import { MsgReceiver } from './msg.receiver'
import { verifyEncoded } from './signature'
import { decodeTxn } from './txn'

export class Node extends MsgReceiver {
  private proxy = createProxy({ db: this.db })

  private check_register_unique = this.db.prepare(/* sql */ `
select id from user
where id = :id
   or username = :username
   or public_key = :public_key
limit 1
`)

  private select_public_key_by_id = this.db
    .prepare(
      /* sql */ `
select public_key from user
where id = :id
limit 1
`,
    )
    .pluck()

  private select_item_owner_by_id = this.db
    .prepare(
      /* sql */ `
select owner_id from item
where id = :item_id
`,
    )
    .pluck()

  private update_owner = this.db.prepare(/* sql */ `
update item
set owner_id = :owner_id
where id = :item_id
`)

  constructor(private keyPair: KeyPair, private db: DBInstance) {
    super()
  }

  async start(options: { port: number }) {
    let server = new IOServer(this)
    await server.start(options.port)
  }

  onReceivedBlockProposal(msg: BlockProposal) {
    this.db.transaction(() => {
      msg.txns.forEach((txnValues) => {
        const txn = decodeTxn(txnValues)
        switch (txn.type) {
          case TxnType.register: {
            if (!verifyEncoded(txnValues, txn.public_key)) {
              throw new Error(`Invalid signature`)
            }
            let user = {
              id: txn.id,
              username: txn.username,
              public_key: txn.public_key,
            }
            let row = this.check_register_unique.get(user)
            if (row) {
              throw new Error('Duplicated user record')
            }
            this.proxy.user.push(user)
            break
          }
          case TxnType.produce: {
            let public_key = this.select_public_key_by_id.get(txn.owner)
            if (!public_key) {
              throw new Error('Unknown user')
            }
            if (!verifyEncoded(txnValues, public_key)) {
              throw new Error('Invalid signature')
            }
            if (this.select_item_owner_by_id.get({ item_id: txn.item })) {
              throw new Error('Duplicated item id')
            }
            this.proxy.item.push({
              id: txn.item,
              owner_id: txn.owner,
            })
            break
          }
          case TxnType.transfer: {
            let owner_id = this.select_item_owner_by_id.get({
              item_id: txn.item,
            })
            if (!owner_id) {
              throw new Error('Item not registered')
            }
            let ownerPublicKey = this.select_public_key_by_id.get(owner_id)
            if (!verifyEncoded(txnValues, ownerPublicKey)) {
              throw new Error('Invalid signature')
            }
            if (!this.select_public_key_by_id.get(txn.receiver)) {
              throw new Error('Receiver is not registered')
            }
            this.update_owner.run({
              owner_id: txn.receiver,
              item_id: txn.item,
            })
            break
          }
          default: {
            console.error('Unknown txn type, txn:', txn)
            throw new TypeError('Unknown txn type')
          }
        }
      })
    })()
  }

  createBlock() {}
}
