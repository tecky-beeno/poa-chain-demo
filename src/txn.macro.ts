import { TxnType, Produce, Register, Transfer } from './data'
import { toName } from './utils'

export namespace sampleTxns {
  let bin = Buffer.alloc(32)

  export let produce: Produce = {
    type: TxnType.produce,
    owner: 1,
    item: 1,
    meta_type: 1,
    meta: bin,
    timestamp: 1,
    signature: bin,
  }

  export let register: Register = {
    type: TxnType.register,
    id: 1,
    public_key: bin,
    username: '',
    timestamp: 1,
    signature: bin,
  }

  export let transfer: Transfer = {
    type: TxnType.transfer,
    item: 1,
    receiver: 2,
    meta_type: 1,
    meta: bin,
    timestamp: 1,
    signature: bin,
  }
}

export const entries = Object.entries(sampleTxns)

entries.forEach(verifySignaturePosition)

/**
 * To make signature generation and verification easier,
 * we should put the signature in the last field of object
 */
export function verifySignaturePosition([name, value]: [
  name: string,
  value: object,
]) {
  const keys = Object.keys(value)

  const fields = ['signature', 'signatures']

  for (let field of fields) {
    const index = keys.indexOf(field)
    if (index === -1) {
      continue // skip signature is absent
    }
    if (index !== keys.length - 1) {
      throw new Error(`'${field}' of '${name}' is not placed in the last field`)
    }
  }
}

export const id_to_Name: string[] = []
Object.values(entries).forEach(([name, sample]) => {
  const id = sample.type
  const Name = toName(name)
  id_to_Name[id] = Name
})

export const Names = Object.keys(sampleTxns).map(toName)
;`
${'i'}mport { Txn, ${Names.join(', ')} } from './data';

${entries
  .map(([name, sample]) => {
    const Name = toName(name)
    const keys = Object.keys(sample).map((key) => `txn.${key}`)
    const tuples = Object.keys(sample).map((key, i) => `${key}:values[${i}]`)
    return `
export function encode${Name}(txn: ${Name}) {
	return [${keys}]
}

export function decode${Name}(values: any[]): ${Name} {
	return {${tuples}}
}
`
  })
  .join('')}

export function encodeTxn(txn: Txn) {
	const type = txn.type
	switch (type) {
${Object.entries(id_to_Name)
  .map(([id, Name]) => `    case ${id}: return encode${Name}(txn)`)
  .join('\n')}
		default: throw new Error('Invalid txn type:' + type)
	}
}

export function decodeTxn(values: any[]) {
	const type = values[0]
	switch (type) {
${Object.entries(id_to_Name)
  .map(([id, Name]) => `    case ${id}: return decode${Name}(values)`)
  .join('\n')}
		default: throw new Error('Invalid txn type:' + type)
	}
}
`.trim() + '\n'
