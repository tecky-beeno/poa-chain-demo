import {
  BlockProposal,
  BlockProposalResponse,
  BlockAnnouncement,
  Nomination,
  NominationResponse,
  NominationAnnouncement,
  Impeachment,
  ImpeachmentResponse,
  ImpeachmentAnnouncement,
} from './data'

export function encodeBlockProposal(msg: BlockProposal) {
  return [
    msg.type,
    msg.prev_hash,
    msg.sender,
    msg.txns,
    msg.timestamp,
    msg.signature,
  ]
}

export function decodeBlockProposal(values: any[]): BlockProposal {
  return {
    type: values[0],
    prev_hash: values[1],
    sender: values[2],
    txns: values[3],
    timestamp: values[4],
    signature: values[5],
  }
}

export function encodeBlockProposalResponse(msg: BlockProposalResponse) {
  return [
    msg.type,
    msg.proposal,
    msg.is_accept,
    msg.sender,
    msg.timestamp,
    msg.signature,
  ]
}

export function decodeBlockProposalResponse(
  values: any[],
): BlockProposalResponse {
  return {
    type: values[0],
    proposal: values[1],
    is_accept: values[2],
    sender: values[3],
    timestamp: values[4],
    signature: values[5],
  }
}

export function encodeBlockAnnouncement(msg: BlockAnnouncement) {
  return [msg.type, msg.proposal, msg.timestamp, msg.signatures]
}

export function decodeBlockAnnouncement(values: any[]): BlockAnnouncement {
  return {
    type: values[0],
    proposal: values[1],
    timestamp: values[2],
    signatures: values[3],
  }
}

export function encodeNomination(msg: Nomination) {
  return [msg.type, msg.sender, msg.target, msg.timestamp, msg.signature]
}

export function decodeNomination(values: any[]): Nomination {
  return {
    type: values[0],
    sender: values[1],
    target: values[2],
    timestamp: values[3],
    signature: values[4],
  }
}

export function encodeNominationResponse(msg: NominationResponse) {
  return [
    msg.type,
    msg.nomination,
    msg.is_accept,
    msg.sender,
    msg.timestamp,
    msg.signature,
  ]
}

export function decodeNominationResponse(values: any[]): NominationResponse {
  return {
    type: values[0],
    nomination: values[1],
    is_accept: values[2],
    sender: values[3],
    timestamp: values[4],
    signature: values[5],
  }
}

export function encodeNominationAnnouncement(msg: NominationAnnouncement) {
  return [msg.type, msg.nomination, msg.timestamp, msg.signatures]
}

export function decodeNominationAnnouncement(
  values: any[],
): NominationAnnouncement {
  return {
    type: values[0],
    nomination: values[1],
    timestamp: values[2],
    signatures: values[3],
  }
}

export function encodeImpeachment(msg: Impeachment) {
  return [msg.type, msg.sender, msg.target, msg.timestamp, msg.signature]
}

export function decodeImpeachment(values: any[]): Impeachment {
  return {
    type: values[0],
    sender: values[1],
    target: values[2],
    timestamp: values[3],
    signature: values[4],
  }
}

export function encodeImpeachmentResponse(msg: ImpeachmentResponse) {
  return [
    msg.type,
    msg.impeachment,
    msg.is_accept,
    msg.sender,
    msg.timestamp,
    msg.signature,
  ]
}

export function decodeImpeachmentResponse(values: any[]): ImpeachmentResponse {
  return {
    type: values[0],
    impeachment: values[1],
    is_accept: values[2],
    sender: values[3],
    timestamp: values[4],
    signature: values[5],
  }
}

export function encodeImpeachmentAnnouncement(msg: ImpeachmentAnnouncement) {
  return [msg.type, msg.impeachment, msg.timestamp, msg.signatures]
}

export function decodeImpeachmentAnnouncement(
  values: any[],
): ImpeachmentAnnouncement {
  return {
    type: values[0],
    impeachment: values[1],
    timestamp: values[2],
    signatures: values[3],
  }
}

export type Message =
  | BlockProposal
  | BlockProposalResponse
  | BlockAnnouncement
  | Nomination
  | NominationResponse
  | NominationAnnouncement
  | Impeachment
  | ImpeachmentResponse
  | ImpeachmentAnnouncement

export function encodeMsg(msg: Message) {
  const type = msg.type
  switch (type) {
    case 1:
      return encodeBlockProposal(msg)
    case 2:
      return encodeBlockProposalResponse(msg)
    case 3:
      return encodeBlockAnnouncement(msg)
    case 4:
      return encodeNomination(msg)
    case 5:
      return encodeNominationResponse(msg)
    case 6:
      return encodeNominationAnnouncement(msg)
    case 7:
      return encodeImpeachment(msg)
    case 8:
      return encodeImpeachmentResponse(msg)
    case 9:
      return encodeImpeachmentAnnouncement(msg)
    default:
      throw new Error('Invalid message type:' + type)
  }
}

export function decodeMsg(values: any[]) {
  const type = values[0]
  switch (type) {
    case 1:
      return decodeBlockProposal(values)
    case 2:
      return decodeBlockProposalResponse(values)
    case 3:
      return decodeBlockAnnouncement(values)
    case 4:
      return decodeNomination(values)
    case 5:
      return decodeNominationResponse(values)
    case 6:
      return decodeNominationAnnouncement(values)
    case 7:
      return decodeImpeachment(values)
    case 8:
      return decodeImpeachmentResponse(values)
    case 9:
      return decodeImpeachmentAnnouncement(values)
    default:
      throw new Error('Invalid message type:' + type)
  }
}
