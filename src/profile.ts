import { newDB } from 'better-sqlite3-schema'
import Knex from 'knex'
import { join } from 'path'
import { createProxy } from './db.proxy'
import { generateKeyPair, saveKey } from './key'

export async function setupProfile(dir: string) {
  let keyFile = join(dir, 'private.key')
  let keyPair = generateKeyPair()
  saveKey(keyFile, keyPair)

  let dbFile = join(dir, 'chain.db')
  let db = newDB({
    path: dbFile,
    migrate: false,
    WAL: true,
  })
  let knex = Knex({
    client: 'better-sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: dbFile,
    },
  })
  await knex.migrate.latest()
  let proxy = createProxy({ db })

  proxy.board_member.push({
    public_key: Buffer.from(keyPair.publicKey),
    referrer_id: null,
  })

  return { db, proxy, knex }
}
