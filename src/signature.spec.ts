import { expect } from 'chai'
import { generateKeyPair } from './key'
import { signEncoded, verifyEncoded } from './signature'

describe('signature TestSuit', () => {
  let publicKey: Buffer
  let privateKey: Buffer
  let values: any[]

  before(() => {
    let keyPair = generateKeyPair()
    publicKey = Buffer.from(keyPair.publicKey)
    privateKey = Buffer.from(keyPair.privateKey)

    values = ['apple', 'price', 123, 3.14, 'signature stub']
  })

  it('should not verify unsigned values', () => {
    expect(() => verifyEncoded(values, publicKey)).to.throw()
  })

  it('should sign the values', () => {
    signEncoded(values, privateKey)
    let signature = values[4]
    expect(signature).not.equals('signature stub')
    expect(signature instanceof Buffer).to.be.true
    expect(signature).to.be.lengthOf(64)
  })

  it('should verify signed values', () => {
    let result = verifyEncoded(values, publicKey)
    expect(result).to.be.true
  })
})
