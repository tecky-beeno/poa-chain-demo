import {
  BlockProposal,
  BlockProposalResponse,
  BlockAnnouncement,
  Nomination,
  NominationResponse,
  NominationAnnouncement,
  Impeachment,
  ImpeachmentResponse,
  ImpeachmentAnnouncement,
} from './data'
import {
  decodeBlockProposal,
  decodeBlockProposalResponse,
  decodeBlockAnnouncement,
  decodeNomination,
  decodeNominationResponse,
  decodeNominationAnnouncement,
  decodeImpeachment,
  decodeImpeachmentResponse,
  decodeImpeachmentAnnouncement,
} from './msg'

export class MsgReceiver {
  onMsgValues(values: any[]) {
    const type = values[0]
    switch (type) {
      case 1:
        this.onBlockProposal(decodeBlockProposal(values))
        return
      case 2:
        this.onBlockProposalResponse(decodeBlockProposalResponse(values))
        return
      case 3:
        this.onBlockAnnouncement(decodeBlockAnnouncement(values))
        return
      case 4:
        this.onNomination(decodeNomination(values))
        return
      case 5:
        this.onNominationResponse(decodeNominationResponse(values))
        return
      case 6:
        this.onNominationAnnouncement(decodeNominationAnnouncement(values))
        return
      case 7:
        this.onImpeachment(decodeImpeachment(values))
        return
      case 8:
        this.onImpeachmentResponse(decodeImpeachmentResponse(values))
        return
      case 9:
        this.onImpeachmentAnnouncement(decodeImpeachmentAnnouncement(values))
        return
    }
  }

  onBlockProposal(msg: BlockProposal) {}
  onBlockProposalResponse(msg: BlockProposalResponse) {}
  onBlockAnnouncement(msg: BlockAnnouncement) {}
  onNomination(msg: Nomination) {}
  onNominationResponse(msg: NominationResponse) {}
  onNominationAnnouncement(msg: NominationAnnouncement) {}
  onImpeachment(msg: Impeachment) {}
  onImpeachmentResponse(msg: ImpeachmentResponse) {}
  onImpeachmentAnnouncement(msg: ImpeachmentAnnouncement) {}
}
