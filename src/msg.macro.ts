import {
  BlockProposal,
  MsgType,
  BlockProposalResponse,
  BlockAnnouncement,
  Nomination,
  NominationResponse,
  NominationAnnouncement,
  Impeachment,
  ImpeachmentResponse,
  ImpeachmentAnnouncement,
} from './data'
import { toName } from './utils'
import { verifySignaturePosition } from './txn.macro'

export namespace sampleMessages {
  let sampleHash = Buffer.alloc(32)
  export let blockProposal: BlockProposal = {
    type: MsgType.blockProposal,
    prev_hash: sampleHash,
    sender: 1,
    txns: [],
    timestamp: 1,
    signature: sampleHash,
  }
  export let blockProposalResponse: BlockProposalResponse = {
    type: MsgType.blockProposalResponse,
    proposal: sampleHash,
    is_accept: 1,
    sender: 1,
    timestamp: 1,
    signature: sampleHash,
  }
  export let blockAnnouncement: BlockAnnouncement = {
    type: MsgType.blockAnnouncement,
    proposal: sampleHash,
    timestamp: 1,
    signatures: [],
  }

  export let nomination: Nomination = {
    type: MsgType.nomination,
    sender: 1,
    target: 1,
    timestamp: 1,
    signature: sampleHash,
  }
  export let nominationResponse: NominationResponse = {
    type: MsgType.nominationResponse,
    nomination: sampleHash,
    is_accept: 1,
    sender: 1,
    timestamp: 1,
    signature: sampleHash,
  }
  export let nominationAnnouncement: NominationAnnouncement = {
    type: MsgType.nominationAnnouncement,
    nomination: sampleHash,
    timestamp: 1,
    signatures: [],
  }

  export let impeachment: Impeachment = {
    type: MsgType.impeachment,
    sender: 1,
    target: 1,
    timestamp: 1,
    signature: sampleHash,
  }
  export let impeachmentResponse: ImpeachmentResponse = {
    type: MsgType.impeachmentResponse,
    impeachment: sampleHash,
    is_accept: 1,
    sender: 1,
    timestamp: 1,
    signature: sampleHash,
  }
  export let impeachmentAnnouncement: ImpeachmentAnnouncement = {
    type: MsgType.impeachmentAnnouncement,
    impeachment: sampleHash,
    timestamp: 1,
    signatures: [],
  }
}

export const entries = Object.entries(sampleMessages)

entries.forEach(verifySignaturePosition)

export const id_to_Name: string[] = []
Object.values(entries).forEach(([name, sample]) => {
  const id = sample.type
  const Name = toName(name)
  id_to_Name[id] = Name
})

export const Names = Object.keys(sampleMessages).map(toName)
;`
${'i'}mport { ${Names.join(', ')} } from './data';

${entries
  .map(([name, sample]) => {
    const Name = toName(name)
    const keys = Object.keys(sample).map((key) => `msg.${key}`)
    const tuples = Object.keys(sample).map((key, i) => `${key}:values[${i}]`)
    return `
export function encode${Name}(msg: ${Name}) {
	return [${keys}]
}

export function decode${Name}(values: any[]): ${Name} {
	return {${tuples}}
}
`
  })
  .join('')}

export type Message = ${Names.join(' | ')}


export function encodeMsg(msg: Message) {
	const type = msg.type
	switch (type) {
${Object.entries(id_to_Name)
  .map(([id, Name]) => `    case ${id}: return encode${Name}(msg)`)
  .join('\n')}
		default: throw new Error('Invalid message type:' + type)
	}
}

export function decodeMsg(values: any[]) {
	const type = values[0]
	switch (type) {
${Object.entries(id_to_Name)
  .map(([id, Name]) => `    case ${id}: return decode${Name}(values)`)
  .join('\n')}
		default: throw new Error('Invalid message type:' + type)
	}
}
`.trim() + '\n'
