import { config } from 'dotenv'
import populateEnv from 'populate-env'

config()

export let env = {}

populateEnv(env, { mode: 'halt' })
