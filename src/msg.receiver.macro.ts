import { id_to_Name, Names } from './msg.macro'
;`
import { ${Names} } from './data'
import { ${Names.map((Name) => `decode${Name}`)} } from './msg'

export class MsgReceiver {
  onMsgValues(values: any[]) {
    const type = values[0]
    switch (type) {
      ${Object.entries(id_to_Name)
        .map(
          ([id, Name]) =>
            `case ${id}: this.on${Name}(decode${Name}(values))
return`,
        )
        .join('\n')}
    }
  }

  ${Names.map((Name) => `on${Name}(msg: ${Name}) {}`).join('\n')}
}
`.trim()
