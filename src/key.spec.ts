import { expect } from 'chai'
import { join } from 'path'
import { generateKeyPair, KeyPair, loadKey, saveKey } from './key'

describe('keyPair encoding / decoding', () => {
  let keyPair: KeyPair

  it('should generate key', () => {
    keyPair = generateKeyPair()
    expect(keyPair).not.undefined
  })

  let keyFile = join('res', 'test.key')

  it('should save key to file', () => {
    saveKey(keyFile, keyPair)
  })

  it('should load key from file', () => {
    let loadedKey = loadKey(keyFile)
    expect(loadedKey).deep.equals(keyPair)
  })
})
