import { readFileSync, writeFileSync } from 'fs'
import forge from 'node-forge'

let ec = forge.pki.ed25519

export type KeyPair = ReturnType<typeof generateKeyPair>

export function generateKeyPair() {
  return ec.generateKeyPair()
}

export function saveKey(file: string, keyPair: KeyPair) {
  let text = keyPair.privateKey.toString('hex')
  writeFileSync(file, text)
}

export function loadKey(file: string): KeyPair {
  let text = readFileSync(file).toString()
  let privateKey = Buffer.from(text, 'hex')
  let publicKey = ec.publicKeyFromPrivateKey({ privateKey })
  return { publicKey, privateKey }
}
