import forge from 'node-forge'
import * as msgpack from '@msgpack/msgpack'

let ec = forge.pki.ed25519

export let emptySignature: Buffer = Buffer.from([])

let signOption = {
  privateKey: Buffer.from([]),
  message: new Uint8Array(0),
  encoding: 'binary',
}

export function signEncoded(values: any[], privateKey: Buffer): any[] {
  let message = msgpack.encode(values.slice(0, values.length - 1))

  signOption.privateKey = privateKey
  signOption.message = message

  let signature = ec.sign(signOption)

  let signatureBuffer: Buffer =
    signature instanceof Buffer ? signature : Buffer.from(signature)

  values[values.length - 1] = signatureBuffer

  return values
}

let verifyOption = {
  publicKey: Buffer.from([]),
  signature: Buffer.from([]),
  message: new Uint8Array(0),
  encoding: 'binary',
}

export function verifyEncoded(values: any[], publicKey: Buffer): boolean {
  let message = msgpack.encode(values.slice(0, values.length - 1))

  verifyOption.message = message
  verifyOption.publicKey = publicKey
  verifyOption.signature = values[values.length - 1]

  return ec.verify(verifyOption)
}
