export type KeyId = number
export type Timestamp = number
export type Url = string
export type Hash = Buffer
export type Bool = 1 | 0
export type PublicKey = Buffer

export enum TxnType {
  produce = 1, // register item
  transfer = 2, // transfer item ownership
  split = 3, // break down item
  invite = 4, // add more authority
  abolish = 5, // kick out authority
  register = 6, // for content creator to obtain key id
}

export type Produce = {
  type: TxnType.produce
  owner: KeyId
  item: KeyId
  meta_type: number
  meta: Buffer
  timestamp: Timestamp
  signature: Hash // signed by owner
}

export type Transfer = {
  type: TxnType.transfer
  item: KeyId
  receiver: KeyId
  meta_type: number
  meta: Buffer
  timestamp: Timestamp
  signature: Hash // signed by sender
}

export type Split = {
  type: TxnType.split
  origin_item: KeyId
  items: {
    owner: KeyId
    item: KeyId
    meta: string
  }[]
  meta_type: number
  meta: string
  timestamp: Timestamp
  signature: Hash // signed by origin_item's owner / transfered receiver
}

export type Invite = {
  type: TxnType.invite
  nomination: Hash
  signatures: Hash[] // signed by multiple authorities
  timestamp: Timestamp
}

export type Abolish = {
  type: TxnType.abolish
  impeachment: Hash
  signatures: Hash[] // signed by multiple authorities
  timestamp: Timestamp
}

export type Register = {
  type: TxnType.register
  id: KeyId
  public_key: PublicKey
  username: string
  signature: Hash
  timestamp: Timestamp
}

export type Txn = Produce | Transfer | Split | Invite | Abolish | Register

export enum MsgType {
  blockProposal = 1,
  blockProposalResponse = 2,
  blockAnnouncement = 3, // when collected enough response

  nomination = 4,
  nominationResponse = 5,
  nominationAnnouncement = 6, // when collected enough response

  impeachment = 7,
  impeachmentResponse = 8,
  impeachmentAnnouncement = 9, // when collected enough response
}

export type ItemMetaData = {
  images: Hash[]
  desc: string
  extra: string
}

export type LatLng = [lat: number, lng: number]

export type BlockProposal = {
  type: MsgType.blockProposal
  prev_hash: Hash
  sender: KeyId
  txns: any[][]
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type BlockProposalResponse = {
  type: MsgType.blockProposalResponse
  proposal: Hash
  is_accept: Bool
  sender: KeyId
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type BlockAnnouncement = {
  type: MsgType.blockAnnouncement
  proposal: Hash
  timestamp: Timestamp
  signatures: Hash[] // signed by majority of authorities
}

export type Nomination = {
  type: MsgType.nomination
  sender: KeyId
  target: KeyId
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type NominationResponse = {
  type: MsgType.nominationResponse
  nomination: Hash
  is_accept: Bool
  sender: KeyId
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type NominationAnnouncement = {
  type: MsgType.nominationAnnouncement
  nomination: Hash
  timestamp: Timestamp
  signatures: Hash[] // signed by majority of authorities
}

export type Impeachment = {
  type: MsgType.impeachment
  sender: KeyId
  target: KeyId
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type ImpeachmentResponse = {
  type: MsgType.impeachmentResponse
  impeachment: Hash
  is_accept: Bool
  sender: KeyId
  timestamp: Timestamp
  signature: Hash // signed by a authority
}

export type ImpeachmentAnnouncement = {
  type: MsgType.impeachmentAnnouncement
  impeachment: Hash
  timestamp: Timestamp
  signatures: Hash[] // signed by majority of authorities
}
