import {
  encode as encodeMsgpack,
  decode as decodeMsgpack,
} from '@msgpack/msgpack'
import debug from 'debug'
import { Server, createServer, connect, Socket } from 'net'
import { encodeMsg, Message } from './msg'
import { MsgReceiver } from './msg.receiver'

export class IOServer {
  server: Server
  connections: IOConnection[] = []
  constructor(public msgReceiver: MsgReceiver) {
    this.server = createServer(this.onClient)
  }
  start(port: number) {
    return new Promise<void>((resolve, reject) => {
      this.server.listen(port, resolve).on('error', reject)
    })
  }
  onClient = (client: Socket) => {
    const id = this.connections.length + 1
    const name = `chain:io:client-${id}`
    this.connections.push(new IOConnection(client, name, this.msgReceiver))
  }
}

export class IOClient {
  server: Socket
  connection: IOConnection
  constructor(port: number, host: string, public msgReceiver: MsgReceiver) {
    this.server = connect(port, host)
    const name = `chain:io:server`
    this.connection = new IOConnection(this.server, name, this.msgReceiver)
  }
  onConnect(cb: () => void) {
    this.server.on('connect', cb)
  }
}

export class IOConnection {
  log = debug(this.name)
  constructor(
    protected socket: Socket,
    public name: string,
    public msgReceiver: MsgReceiver,
  ) {
    // this.log.enabled = true;
    this.log('connection created')
    socket.on('data', (bin) => {
      this.log('received bin:', bin)
      // this.log('received string:', bin.toString('binary'))
      const values = decodeMsgpack(bin)
      // this.log('decoded:', values)
      if (!Array.isArray(values)) {
        this.socket.destroy()
        return
      }
      this.msgReceiver.onMsgValues(values)
      // const msg = decodeMsg(values)
      // this.log('received message:', msg)
    })
  }
  sendMessage(msg: Message) {
    const values = encodeMsg(msg)
    const bin = encodeMsgpack(values)
    this.socket.write(bin)
  }
}
