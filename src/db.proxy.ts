import { proxySchema, ProxySchemaOptions } from 'better-sqlite3-proxy'

export type BoardMember = {
  id?: number | null
  public_key: Buffer
  referrer_id: number | null
  referrer?: BoardMember
}

export type User = {
  id?: number | null
  public_key: Buffer
  username: string
}

export type Item = {
  id?: number | null
  owner_id: number
  owner?: User
}

export type Block = {
  id?: number | null
  hash: Buffer
  sender_id: number
  sender?: BoardMember
  propose_time: number
  confirm_time: number
  txns: Buffer
  signatures: Buffer
}

export type DBProxy = {
  board_member: BoardMember[]
  user: User[]
  item: Item[]
  block: Block[]
}

export let tableFields: ProxySchemaOptions<DBProxy>['tableFields'] = {
  board_member: [
    /* foreign references */
    ['referrer', { field: 'referrer_id', table: 'board_member' }],
  ],
  user: [],
  item: [
    /* foreign references */
    ['owner', { field: 'owner_id', table: 'user' }],
  ],
  block: [
    /* foreign references */
    ['sender', { field: 'sender_id', table: 'board_member' }],
  ],
}

export function createProxy(
  options: Omit<ProxySchemaOptions<DBProxy>, 'tableFields'>,
) {
  return proxySchema<DBProxy>({
    tableFields,
    ...options,
  })
}
