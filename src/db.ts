import { toSafeMode, newDB } from 'better-sqlite3-schema'

export let dbFile = 'chain.db'

export let db = newDB({
  path: dbFile,
  migrate: false,
})

toSafeMode(db)
