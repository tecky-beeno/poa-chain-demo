import { Txn, Produce, Register, Transfer } from './data'

export function encodeProduce(txn: Produce) {
  return [
    txn.type,
    txn.owner,
    txn.item,
    txn.meta_type,
    txn.meta,
    txn.timestamp,
    txn.signature,
  ]
}

export function decodeProduce(values: any[]): Produce {
  return {
    type: values[0],
    owner: values[1],
    item: values[2],
    meta_type: values[3],
    meta: values[4],
    timestamp: values[5],
    signature: values[6],
  }
}

export function encodeRegister(txn: Register) {
  return [
    txn.type,
    txn.id,
    txn.public_key,
    txn.username,
    txn.timestamp,
    txn.signature,
  ]
}

export function decodeRegister(values: any[]): Register {
  return {
    type: values[0],
    id: values[1],
    public_key: values[2],
    username: values[3],
    timestamp: values[4],
    signature: values[5],
  }
}

export function encodeTransfer(txn: Transfer) {
  return [
    txn.type,
    txn.item,
    txn.receiver,
    txn.meta_type,
    txn.meta,
    txn.timestamp,
    txn.signature,
  ]
}

export function decodeTransfer(values: any[]): Transfer {
  return {
    type: values[0],
    item: values[1],
    receiver: values[2],
    meta_type: values[3],
    meta: values[4],
    timestamp: values[5],
    signature: values[6],
  }
}

export function encodeTxn(txn: Txn) {
  const type = txn.type
  switch (type) {
    case 1:
      return encodeProduce(txn)
    case 2:
      return encodeTransfer(txn)
    case 6:
      return encodeRegister(txn)
    default:
      throw new Error('Invalid txn type:' + type)
  }
}

export function decodeTxn(values: any[]) {
  const type = values[0]
  switch (type) {
    case 1:
      return decodeProduce(values)
    case 2:
      return decodeTransfer(values)
    case 6:
      return decodeRegister(values)
    default:
      throw new Error('Invalid txn type:' + type)
  }
}
