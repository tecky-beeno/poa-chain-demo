#!/usr/bin/env ts-node
import { catchMain } from '@beenotung/tslib/node'
import { accessSync, mkdirSync } from 'fs'
import { setupProfile } from '../src/profile'

async function main() {
  let profileDir = process.argv[2]
  if (!profileDir) {
    console.error('Error: missing profile directory in the argument')
    process.exit(1)
  }

  try {
    accessSync(profileDir)
    console.error(
      'Error: the profile directory already exists, refuse to overwrite it',
    )
    process.exit(1)
  } catch (error) {
    console.log('Going to save new profile to:', profileDir)
  }

  mkdirSync(profileDir)
  const { knex } = await setupProfile(profileDir)
  await knex.destroy()
}

catchMain(main())
