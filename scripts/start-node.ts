#!/usr/bin/env ts-node

import { loadKey } from '../src/key'
import { Node } from '../src/node'
import { catchMain } from '@beenotung/tslib/node'
import { print } from 'listening-on'
import { join } from 'path'
import { newDB } from 'better-sqlite3-schema'
import Knex from 'knex'

let port = 8100
let profileDir = ''

for (let i = 2; i < process.argv.length; i++) {
  let arg = process.argv[i]
  switch (arg) {
    case '--dir': {
      i++
      profileDir = process.argv[i]
      if (!profileDir) {
        console.error('Error: missing profile directory in the argument')
        process.exit(1)
      }
      break
    }
    case '--port': {
      i++
      port = +process.argv[i]
      if (!port) {
        console.error('Error: missing/invalid port in the argument')
        process.exit(1)
      }
      break
    }
    default: {
      console.error('Error: invalid argument:', arg)
      process.exit(1)
    }
  }
}

if (!profileDir) {
  console.error('Error: missing key name in the argument')
  process.exit(1)
}

let keyFile = join(profileDir, 'private.key')
let keyPair = loadKey(keyFile)

let dbFile = profileDir + '/chain.db'
let db = newDB({
  path: dbFile,
})
let knex = Knex({
  client: 'better-sqlite3',
  connection: {
    filename: dbFile,
  },
})

async function main() {
  await knex.migrate.latest()
  let node = new Node(keyPair, db)
  node.start({ port })
  print({ port, protocol: 'tcp' })
}

catchMain(main())
