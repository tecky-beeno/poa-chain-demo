import debug from 'debug'
import { Debugger } from 'debug'
import {
  BlockAnnouncement,
  BlockProposal,
  BlockProposalResponse,
  Impeachment,
  ImpeachmentAnnouncement,
  ImpeachmentResponse,
  MsgType,
  Nomination,
  NominationAnnouncement,
  NominationResponse,
} from '../src/data'
import { IOServer, IOClient } from '../src/io'
import { MsgReceiver } from '../src/msg.receiver'

class LogReceiver extends MsgReceiver {
  log: Debugger

  constructor(name: string) {
    super()
    this.log = debug(name)
    this.log.enabled = true
  }

  onBlockProposal(msg: BlockProposal): void {
    this.log(msg)
  }
  onBlockProposalResponse(msg: BlockProposalResponse): void {
    this.log(msg)
  }
  onBlockAnnouncement(msg: BlockAnnouncement): void {
    this.log(msg)
  }
  onNomination(msg: Nomination): void {
    this.log(msg)
  }
  onNominationResponse(msg: NominationResponse): void {
    this.log(msg)
  }
  onNominationAnnouncement(msg: NominationAnnouncement): void {
    this.log(msg)
  }
  onImpeachment(msg: Impeachment): void {
    this.log(msg)
  }
  onImpeachmentResponse(msg: ImpeachmentResponse): void {
    this.log(msg)
  }
  onImpeachmentAnnouncement(msg: ImpeachmentAnnouncement): void {
    this.log(msg)
  }
}

let server = new IOServer(new LogReceiver('server'))
let serverPort = 8100
let serverHost = 'localhost'

async function main() {
  await server.start(serverPort)
  let client = new IOClient(serverPort, serverHost, new LogReceiver('client'))
  client.onConnect(() => {
    client.connection.sendMessage({
      type: MsgType.nomination,
      sender: 1,
      target: 1,
      timestamp: Date.now(),
      signature: Buffer.from('sample hash'),
    })
  })
}
main().catch((e) => console.error(e))
