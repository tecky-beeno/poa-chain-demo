import forge from 'node-forge'

let ec = forge.pki.ed25519

let keypair = ec.generateKeyPair()

let { publicKey, privateKey } = keypair

console.log('private key size:', privateKey.length)
console.log('public key size:', publicKey.length)

let msg = '123'

let signature = ec.sign({
  message: msg,
  encoding: 'utf8',
  privateKey,
})

console.log('signature b64:', signature.toString('base64'))
console.log('signature b16:', signature.toString('hex'))
console.log('signature length:', signature.length)

let fakeMsg = '123.0'

let verified = ec.verify({
  message: fakeMsg,
  encoding: 'utf8',
  signature,
  publicKey,
})

console.log({ verified })

console.log({
  publicKey: Array.from(publicKey),
  privateKey: Array.from(privateKey),
})
