import forge from 'node-forge'

let rsa = forge.pki.rsa

let md = forge.md

let { sha256 } = md

let keypair = rsa.generateKeyPair({ bits: 2048, e: 0x10001 })

let { publicKey, privateKey } = keypair

console.log('key size:', 2048 / 8)

let msg = '123'

let msgDigest = sha256.create().update(msg, 'utf8')

console.log('msg digest:', msgDigest.digest().toHex())

let signature = privateKey.sign(msgDigest)
console.log(
  'msg signature:',
  Buffer.from(signature, 'binary').toString('base64'),
)
console.log('signature length:', signature.length)

let fakeMsg = '123.0'
let fakeDigest = sha256.create().update(fakeMsg, 'utf8')
console.log('fake digest:', fakeDigest.digest().toHex())
let verified = publicKey.verify(fakeDigest.digest().bytes(), signature)

console.log({ verified })
