import { Produce, Register, Transfer, Txn, TxnType } from '../src/data'
import { emptySignature, signEncoded } from '../src/signature'
import { encodeProduce, encodeRegister, encodeTransfer } from '../src/txn'

let aliceKeyPair = {
  publicKey: Buffer.from([
    188, 68, 225, 116, 225, 12, 240, 86, 179, 130, 78, 113, 116, 8, 16, 45, 194,
    110, 186, 64, 79, 252, 196, 153, 186, 12, 104, 93, 106, 83, 117, 17,
  ]),
  privateKey: Buffer.from([
    111, 206, 123, 154, 155, 209, 114, 246, 224, 218, 88, 80, 43, 156, 148, 148,
    61, 131, 119, 229, 239, 27, 127, 36, 236, 149, 50, 149, 231, 156, 28, 12,
    188, 68, 225, 116, 225, 12, 240, 86, 179, 130, 78, 113, 116, 8, 16, 45, 194,
    110, 186, 64, 79, 252, 196, 153, 186, 12, 104, 93, 106, 83, 117, 17,
  ]),
}

let bobKeyPair = {
  publicKey: Buffer.from([
    240, 184, 144, 71, 197, 84, 250, 191, 254, 11, 237, 2, 104, 108, 78, 93, 84,
    21, 83, 174, 35, 249, 69, 195, 54, 175, 173, 122, 65, 14, 118, 9,
  ]),
  privateKey: Buffer.from([
    64, 194, 140, 29, 186, 102, 4, 149, 158, 105, 59, 106, 240, 227, 53, 199,
    179, 222, 81, 136, 142, 58, 173, 224, 162, 176, 69, 230, 74, 90, 76, 6, 240,
    184, 144, 71, 197, 84, 250, 191, 254, 11, 237, 2, 104, 108, 78, 93, 84, 21,
    83, 174, 35, 249, 69, 195, 54, 175, 173, 122, 65, 14, 118, 9,
  ]),
}

let sampleTxns: any[][] = []

let registerAlice: Register = {
  type: TxnType.register,
  id: 1,
  public_key: aliceKeyPair.publicKey,
  username: 'alice',
  signature: emptySignature,
  timestamp: new Date('2022-07-08 23:31').getTime(),
}
sampleTxns.push(
  signEncoded(encodeRegister(registerAlice), aliceKeyPair.privateKey),
)

let registerBob: Register = {
  type: TxnType.register,
  id: 2,
  public_key: bobKeyPair.publicKey,
  username: 'bob',
  signature: emptySignature,
  timestamp: new Date('2022-07-08 23:31').getTime(),
}
sampleTxns.push(signEncoded(encodeRegister(registerBob), bobKeyPair.privateKey))

let produce: Produce = {
  type: TxnType.produce,
  owner: 1,
  item: 1,
  meta_type: 1,
  meta: Buffer.from('mint from thin air'),
  timestamp: new Date('2022-07-08 23:31').getTime(),
  signature: emptySignature,
}
sampleTxns.push(signEncoded(encodeProduce(produce), aliceKeyPair.privateKey))

let transfer: Transfer = {
  type: TxnType.transfer,
  item: 1,
  receiver: 2,
  meta_type: 1,
  meta: Buffer.from('gift to you'),
  timestamp: new Date('2022-07-08 23:31').getTime(),
  signature: emptySignature,
}
sampleTxns.push(signEncoded(encodeTransfer(transfer), aliceKeyPair.privateKey))
