import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable('board_member'))) {
    await knex.schema.createTable('board_member', (table) => {
      table.increments('id')
      table.binary('public_key').notNullable()
      table
        .integer('referrer_id')
        .unsigned()
        .nullable()
        .references('board_member.id')
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', (table) => {
      table.increments('id')
      table.binary('public_key').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('item'))) {
    await knex.schema.createTable('item', (table) => {
      table.increments('id')
      table.integer('owner_id').unsigned().notNullable().references('user.id')
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('block'))) {
    await knex.schema.createTable('block', (table) => {
      table.increments('id')
      table.binary('hash').notNullable()
      table
        .integer('sender_id')
        .unsigned()
        .notNullable()
        .references('board_member.id')
      table.integer('propose_time').notNullable()
      table.integer('confirm_time').notNullable()
      table.binary('txns').notNullable()
      table.binary('signatures').notNullable()
      table.timestamps(false, true)
    })
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('block')
  await knex.schema.dropTableIfExists('item')
  await knex.schema.dropTableIfExists('user')
  await knex.schema.dropTableIfExists('board_member')
}
